package ru.perm.v;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        User user = User.builder().name("vasi").age(52).build();
        System.out.println(user);

        User u = User.builder().name("Anna").build();
        System.out.println(u);

        User u1 = new User("aaa",0);
        System.out.println(u1);
    }
}
