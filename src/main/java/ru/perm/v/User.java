package ru.perm.v;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@Builder(toBuilder = true)
//@RequiredArgsConstructor
public class User {
    String name;
    @Builder.Default
    Integer age = 0;
}
