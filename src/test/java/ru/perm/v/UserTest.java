package ru.perm.v;

import org.junit.Test;

public class UserTest {

    @Test
    public void builder() {
        User user = User.builder().name("vasi").age(52).build();
        assert user.getName().equals("vasi");
        assert user.getAge() == 52;
    }

    @Test
    public void constructor() {
        User user = new User("vasi", 52);
        assert user.getName().equals("vasi");
        assert user.getAge() == 52;
    }

    @Test
    public void defaultValue() {
        User user = User.builder().name("vasi").build();
        assert user.getName().equals("vasi");
        assert user.getAge() == 0;
    }
}
